import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec

class WordWrapSpec extends AnyFunSpec with GivenWhenThen {

  /*
  def fixture: Object {
    val n: Int
    val shortText: String
    val longText: String
  } =
    new {
      val n = 10
      val shortText = "Amen"
      val longText = "La procellosa e trepida gioia d'un gran disegno"
    }

   */

  case class Fixture(n: Int = 10, shortText: String = "Amen", longText: String = "La procellosa e trepida gioia d'un gran disegno", brokenText: String = "La procellosa e\r\ntrepida gioia\r\nd'un gran disegno")

  describe("A short string") {

    val f = Fixture()

    it("should be short") {
      assert(f.shortText.length <= f.n)
    }

    it("should not include long words") {
      assert(!f.shortText.split(" ").exists(_.length > f.n))
    }

    it("should be left untouched") {
      import WordWrap.StringImprovements
      assert(f.shortText.wordWrap(f.n) == f.shortText)
    }
  }

  describe("A long string") {

    val f = Fixture()

    it("should be long") {
      assert(f.longText.length > f.n)
    }

    it("should not include long words") {
      assert(!f.longText.split(" ").exists(_.length > f.n))
    }

    it("should undergo word wrapping from left to right, only where strictly necessary") {
      import WordWrap.StringImprovements
      assert(f.longText.wordWrap(f.n) ==
        """La
          |procellosa
          |e trepida
          |gioia d'un
          |gran
          |disegno""".stripMargin)
    }
  }

  describe("A broken string") {

    val f = Fixture()

    it("should include at least one newline") {
      assert(f.brokenText.indexOf("\r\n") > 0)
    }

    it("should not include long words") {
      assert(!f.brokenText.split("\r\n").flatMap(_.split(" ")).exists(_.length > f.n))
    }

    it("should keep its existing newlines") {
      import WordWrap.StringImprovements
      assert(f.brokenText.prettyWordWrap(f.n) ==
        """La
          |procellosa
          |e
          |trepida
          |gioia
          |d'un gran
          |disegno""".stripMargin)
    }
  }
}

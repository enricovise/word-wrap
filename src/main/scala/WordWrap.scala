object WordWrap {

  implicit class StringImprovements(aString: String) {

    def prettyWordWrap(anInt: Int): String = {
      this.aString.split("\r\n").map(_.wordWrap(anInt)).mkString("\r\n")
    }

    def wordWrap(anInteger: Int): String = {
      if (this.aString.length <= anInteger) {
        return this.aString
      }

      val wrapPlace = this.aString.substring(0, anInteger + 1).lastIndexOf(' ')

      s"${this.aString.substring(0, wrapPlace)}\r\n${this.aString.substring(wrapPlace + 1).wordWrap(anInteger)}"
    }

  }
}
